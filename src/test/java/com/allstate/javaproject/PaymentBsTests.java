package com.allstate.javaproject;

import com.allstate.javaproject.entities.Payment;
import com.allstate.javaproject.services.PaymentService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.MongoTemplate;

import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest
public class PaymentBsTests {

    @Autowired
    private MongoTemplate mongoTemplate;

    @Autowired
    private PaymentService service;

    @BeforeEach
    public void cleanup() {
        mongoTemplate.dropCollection(Payment.class);
    }

    @Test
    public void rowCountReturnsCountOfPaymentsInCollection() {
        service.save(new Payment(1, new Date(), "cash", 1000.00, 1111));
        service.save(new Payment(2, new Date(), "cheque", 2000.00, 2222));
        service.save(new Payment(3, new Date(), "card", 3000.00, 3333));

        assertEquals(3, service.rowCount());
    }

    @Test
    public void findByIdThrowsExceptionWhenIdIsLessThanZero() {
        Assertions.assertThrows(IllegalArgumentException.class, () -> service.findById(-1));
    }

    @Test
    public void findByIdReturnsExpectedPayment() {

        service.save(new Payment(1, new Date(), "cash", 1000.00, 1111));

        Payment foundPayment = service.findById(1);

        assertNotNull(foundPayment);
        assertEquals(1, foundPayment.getId());
    }

    @Test
    public void findByTypeThrowsExceptionWhenTypeIsNull() {
        Assertions.assertThrows(IllegalArgumentException.class, () -> service.findByType(null));
    }

    @Test
    public void findByTypeReturnsAllMatchingPayments() {

        service.save(new Payment(1, new Date(), "cash", 1000.00, 1111));
        service.save(new Payment(2, new Date(), "cash", 2000.00, 2222));
        service.save(new Payment(3, new Date(), "card", 3000.00, 3333));

        List<Payment> payments = service.findByType("cash");

        assertEquals(2, payments.size());
    }

    @Test
    public void findAllReturnsAllPayments() {

        service.save(new Payment(1, new Date(), "cash", 1000.00, 1111));
        service.save(new Payment(2, new Date(), "cash", 2000.00, 2222));
        service.save(new Payment(3, new Date(), "card", 3000.00, 3333));

        List<Payment> payments = service.findAll();

        assertEquals(3, payments.size());
    }

    @Test
    public void saveInsertsPaymentIntoTheDatabase() {

        int newId = service.save(new Payment(1, new Date(), "cash", 1000.00, 1111));

        assertEquals(1, service.rowCount());
        assertEquals(1, newId);
    }
}
