package com.allstate.javaproject;

import com.allstate.javaproject.dao.PaymentDao;
import com.allstate.javaproject.entities.Payment;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.MongoTemplate;

import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest
public class PaymentDaoMongoTests {

    @Autowired
    private MongoTemplate mongoTemplate;

    @Autowired
    private PaymentDao paymentDao;

    @BeforeEach
    public void cleanup() {
        mongoTemplate.dropCollection(Payment.class);
    }

    @Test
    public void rowCountReturnsCountOfPaymentsInCollection() {


        paymentDao.save(new Payment(1, new Date(), "cash", 1000.00, 1111));
        paymentDao.save(new Payment(2, new Date(), "cheque", 2000.00, 2222));
        paymentDao.save(new Payment(3, new Date(), "card", 3000.00, 3333));

        assertEquals(3, paymentDao.rowCount());
    }

    @Test
    public void findByIdReturnsExpectedPayment() {

        paymentDao.save(new Payment(1, new Date(), "cash", 1000.00, 1111));

        Payment foundPayment = paymentDao.findById(1);

        assertNotNull(foundPayment);
        assertEquals(1, foundPayment.getId());
    }

    @Test
    public void findByTypeReturnsAllMatchingPayments() {

        paymentDao.save(new Payment(1, new Date(), "cash", 1000.00, 1111));
        paymentDao.save(new Payment(2, new Date(), "cash", 2000.00, 2222));
        paymentDao.save(new Payment(3, new Date(), "card", 3000.00, 3333));

        List<Payment> payments = paymentDao.findByType("cash");

        assertEquals(2, payments.size());
    }

    @Test
    public void findAllReturnsAllPayments() {

        paymentDao.save(new Payment(1, new Date(), "cash", 1000.00, 1111));
        paymentDao.save(new Payment(2, new Date(), "cash", 2000.00, 2222));
        paymentDao.save(new Payment(3, new Date(), "card", 3000.00, 3333));

        List<Payment> payments = paymentDao.findAll();

        assertEquals(3, payments.size());
    }

    @Test
    public void saveInsertsPaymentIntoTheDatabase() {

        int newId = paymentDao.save(new Payment(1, new Date(), "cash", 1000.00, 1111));

        assertEquals(1, paymentDao.rowCount());
        assertEquals(1, newId);
    }
}
