package com.allstate.javaproject;

import com.allstate.javaproject.entities.Payment;
import org.junit.jupiter.api.Test;

import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PaymentTests {

    @Test
    public void constructorInitializesPaymentObjectWithExpectedValues() {

        int id = 123;

        Date paymentDate = new Date();
        String type = "cash";
        double amount = 2000.0;
        int custId = 987654321;

        Payment payment = new Payment(id, paymentDate, type, amount, custId);

        assertEquals(id, payment.getId());
        assertEquals(paymentDate, payment.getPaymentDate());
        assertEquals(type, payment.getType());
        assertEquals(amount, payment.getAmount());
        assertEquals(custId, payment.getCustId());
    }

    @Test
    public void toStringReturnsExpectedValue() {

        // Construct the Payment object
        int id = 123;
        Date paymentDate = new Date();
        String type = "cash";
        double amount = 2000.0;
        int custId = 987654321;

        Payment payment = new Payment(id, paymentDate, type, amount, custId);

        String expectedValue = "Id: " + id + ", Payment Date: " + paymentDate + ", Type: " + type + ", Amount: " + amount + ", Cust ID: " + custId;

        assertEquals(expectedValue,  payment.toString());
    }
}
