package com.allstate.javaproject.dao;

import com.allstate.javaproject.entities.Payment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class PaymentDaoMongo implements PaymentDao {

    @Autowired
    private MongoTemplate mongoTemplate;

    @Override
    public int rowCount() {
        return (int)mongoTemplate.count(new Query(), Payment.class);
    }

    @Override
    public Payment findById(int id) {
        return mongoTemplate.findById(id, Payment.class);
    }

    @Override
    public List<Payment> findByType(String type) {
        Query query = new Query();
        query.addCriteria(Criteria.where("type").is(type));
        return mongoTemplate.find(query, Payment.class);
    }

    @Override
    public List<Payment>findAll() {
        return mongoTemplate.findAll(Payment.class);
    }

    @Override
    public int save(Payment payment) {
        int newId = rowCount() + 1;
        payment.setId(newId);

        mongoTemplate.insert(payment);

        return newId;
    }
}