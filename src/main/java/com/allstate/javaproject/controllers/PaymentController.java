package com.allstate.javaproject.controllers;

import com.allstate.javaproject.entities.Payment;
import com.allstate.javaproject.services.PaymentService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/payments")
public class PaymentController {

    private static final Logger LOGGER = LoggerFactory.getLogger(PaymentController.class);

    @Autowired
    private PaymentService service;
    
    @RequestMapping(value = "/status", method = RequestMethod.GET)
    public String status() {
        LOGGER.info("/status endpoint was called.");
        return "Payment API is running.";
    }

    @RequestMapping(value = "/count", method = RequestMethod.GET)
    public int rowCount() {
        int count = service.rowCount();

        LOGGER.info("/count endpoint was called. Count = " + count);

        return count;
    }

    @RequestMapping(value = "/findById/{id}", method = RequestMethod.GET)
    public ResponseEntity<Payment> findById(@PathVariable("id") int id) {

        LOGGER.info("/findById endpoint was called for id = " + id);

        try {
            Payment payment = service.findById(id);

            if(payment != null) {
                LOGGER.info("Payment found: " + payment.toString());
                return new ResponseEntity<Payment>(payment, HttpStatus.OK);
            }
            else {
                LOGGER.info("No payment found for id.");
                return new ResponseEntity<Payment>(HttpStatus.NOT_FOUND);
            }
        }
        catch (Exception e) {
            return new ResponseEntity<Payment>(HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(value = "/findByType/{type}", method = RequestMethod.GET)
    public ResponseEntity<List<Payment>> findByType(@PathVariable("type") String type) {

        LOGGER.info("/findByType endpoint was called for type = " + type);

        try {
            List<Payment> payments = service.findByType(type);

            LOGGER.info("Payments found = " + payments.size());

            return new ResponseEntity<List<Payment>>(payments, HttpStatus.OK);
        }
        catch (Exception e) {
            return new ResponseEntity<List<Payment>>(HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(value = "/all", method = RequestMethod.GET)
    public List<Payment> findAll() {
        return service.findAll();
    }

    @RequestMapping(method = RequestMethod.POST)
    public int save(@RequestBody Payment payment) {
        int newId = service.save(payment);

        LOGGER.info("Payment Saved: " + payment.toString());

        return newId;
    }
}
