package com.allstate.javaproject.services;

import com.allstate.javaproject.dao.PaymentDao;
import com.allstate.javaproject.entities.Payment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PaymentBs implements PaymentService {

    @Autowired
    PaymentDao paymentDao;

    @Override
    public int rowCount() {
        return paymentDao.rowCount();
    }

    @Override
    public Payment findById(int id) {

        if(id > 0) {
            return paymentDao.findById(id);
        }
        else {
            throw new IllegalArgumentException("The id value must be greater than zero.");
        }
    }

    @Override
    public List<Payment> findByType(String type) {

        if(type != null) {
            return paymentDao.findByType(type);
        }
        else {
            throw new IllegalArgumentException("The type parameter cannot be null.");
        }
    }

    @Override
    public List<Payment> findAll() {
        return paymentDao.findAll();
    }

    @Override
    public int save(Payment payment) {
        return paymentDao.save(payment);
    }
}
