package com.allstate.javaproject.services;

import com.allstate.javaproject.entities.Payment;

import java.util.List;

public interface PaymentService {
    int rowCount();
    Payment findById(int id);
    List<Payment> findByType(String type);
    List<Payment> findAll();
    int save(Payment payment);
}
